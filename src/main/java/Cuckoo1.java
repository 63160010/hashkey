/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Acer
 */
public class Cuckoo1 {
    private int K;
    private String V;
    private int n = 50;
    Cuckoo1[] T1 = new Cuckoo1[n];
    Cuckoo1[] T2 = new Cuckoo1[n];
    public Cuckoo1() {
    }
    public Cuckoo1(int K, String V) {
        this.K = K;
        this.V = V;
    }
    public int getKey() {
        return K;
    }
    public String getValue() {
        return V;
    }
     @Override
    public String toString() {
        return "Key= " + K + " Value=" + V;
    }
    public int hash(int K) {
        return K % T1.length;
    }
    public int hash1(int K) {
        return (K / n) % T2.length;
    }
    public void putOff(int K, String V) {
        if (T1[hash(K)] != null && T1[hash(K)].K == K) {
            T1[hash(K)].K = K;
            T1[hash(K)].V = V;
            return;
        }
        if (T2[hash1(K)] != null && T2[hash1(K)].K == K) {
            T2[hash1(K)].K = K;
            T2[hash1(K)].V = V;
            return;
        }
        int i = 0;
        while (true) {
            if (i == 0) {
                if (T1[hash(K)] == null) {
                    T1[hash(K)] = new Cuckoo1();
                    T1[hash(K)].K = K;
                    T1[hash(K)].V = V;
                    return;
                }
            } else {
                if (T2[hash1(K)] == null) {
                    T2[hash1(K)] = new Cuckoo1();
                    T2[hash1(K)].K = K;
                    T2[hash1(K)].V = V;
                    return;
                }
            }
            if (i == 0) {
                String tempValue = T1[hash(K)].V;
                int tempKey = T1[hash(K)].K;
                T1[hash(K)].K = K;
                T1[hash(K)].V = V;
                K = tempKey;
                V = tempValue;
            } else {
                String tempValue = T2[hash1(K)].V;
                int tempKey = T2[hash1(K)].K;
                T2[hash1(K)].K = K;
                T2[hash1(K)].V = V;
                K = tempKey;
                V = tempValue;
            }
            i = (i + 1) % 2;
        }
    }
    public void add(int K, String V) {
        if (T1[hash(K)] != null && T1[hash(K)].K == K) {
            T1[hash(K)].K = K;
            T1[hash(K)].V = V;
            return;
        }
        if (T2[hash1(K)] != null && T2[hash1(K)].K == K) {
            T2[hash1(K)].K = K;
            T2[hash1(K)].V = V;
            return;
        }
        int i = 0;
        while (true) {
            if (i == 0) {
                if (T1[hash(K)] == null) {
                    T1[hash(K)] = new Cuckoo1();
                    T1[hash(K)].K = K;
                    T1[hash(K)].V = V;
                    return;
                }
            } else {
                if (T2[hash1(K)] == null) {
                    T2[hash1(K)] = new Cuckoo1();
                    T2[hash1(K)].K = K;
                    T2[hash1(K)].V = V;
                    return;
                }
            }
            if (i == 0) {
                String tempValue = T1[hash(K)].V;
                int tempKey = T1[hash(K)].K;
                T1[hash(K)].K = K;
                T1[hash(K)].V = V;
                K = tempKey;
                V = tempValue;
            } else {
                String tempValue = T2[hash1(K)].V;
                int tempKey = T2[hash1(K)].K;
                T2[hash1(K)].K = K;
                T2[hash1(K)].V = V;
                K = tempKey;
                V = tempValue;
            }
            i = (i + 1) % 2;
        }
    }
    public Cuckoo1 get(int K) {
        if (T1[hash(K)] != null && T1[hash(K)].K == K) {
            return T1[hash(K)];
        }
        if (T2[hash1(K)] != null && T2[hash1(K)].K == K) {
            return T2[hash1(K)];
        }
        return null;
    }
    public Cuckoo1 delete(int K) {
        if (T1[hash(K)] != null && T1[hash(K)].K == K) {
            Cuckoo1 tmp = T1[hash(K)];
            T1[hash(K)] = null;
            return tmp;
        }
        if (T1[hash1(K)] != null && T1[hash1(K)].K == K) {
            Cuckoo1 tmp = T1[hash1(K)];
            T1[hash1(K)] = null;
            return tmp;
        }
        return null;
    }
    
    public static void main(String[] args) {
        Cuckoo1 cuckoo = new Cuckoo1();
        cuckoo.add(2, "MOOK");
        cuckoo.add(4, "SOI");
        cuckoo.add(6, "MEW");
        
        cuckoo.putOff(2, "KEW");
        cuckoo.putOff(10, "ZACK");
        
        System.out.println(cuckoo.get(2));
        System.out.println(cuckoo.get(3));
        System.out.println(cuckoo.get(4));
        System.out.println(cuckoo.get(6));
        System.out.println(cuckoo.get(10));
        System.out.println("---------");
        cuckoo.delete(10);
        System.out.println(cuckoo.get(10));

    }
}
